
Requirements
 - PHP >= 7.2.5
 - Composer

Setup the project
 - run ``composer dump-autoload``
 - run ``php artisan migrate``
 - run ``php artisan db:seed``