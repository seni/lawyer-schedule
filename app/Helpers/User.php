<?php
declare(strict_types=1);

namespace App\Helpers;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use App\User as UserModel;
use App\Role;

class User
{
    /**
     * @return Collection
     */
    public static function getUserRoles(): Collection
    {
        $currentUserId = Auth::id();
        return UserModel::with('roles')->find($currentUserId)->roles->pluck('slug');
    }

    /**
     * @return Collection
     */
    public static function getPermissions(): Collection
    {
        $permissions = new Collection();
        $slugs = self::getUserRoles();

        $permissionsByRole = $slugs->map(function ($slug) {
            return Role::where('slug', $slug)->first()->permissions;
        });

        foreach ($permissionsByRole as $collection) {
            $permissions = $permissions->merge($collection);
        }

        return $permissions;
    }
}
