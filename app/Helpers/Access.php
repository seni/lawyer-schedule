<?php
declare(strict_types=1);

namespace App\Helpers;

use Illuminate\Http\Request;

class Access
{
    private $adminSlug = 'admin';

    private $clientSlug;


    public function __construct(string $clientSlug = '')
    {
        $this->clientSlug = $clientSlug;
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return mixed
     */
    public function checkPermissions(Request $request)
    {
        $userRoles = User::getUserRoles();
        $userPermissions = User::getPermissions();
        $currentRouteName = $request->route()->getName();

        $accessAllowed = $userPermissions->first(function ($value, $routeName) use ($currentRouteName) {
            return $routeName === $currentRouteName;
        });

        $userRole = $userRoles->first(function ($role) {
            if (!empty($this->clientSlug)) {
                return $role === $this->adminSlug || $role === $this->clientSlug;
            }
            return $role === $this->adminSlug;
        });

        if (empty($userRole) || !$accessAllowed) {
            return false;
        }

        return true;
    }
}
