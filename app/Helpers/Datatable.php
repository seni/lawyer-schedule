<?php
declare(strict_types=1);

namespace App\Helpers;

use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Illuminate\Http\JsonResponse;

class Datatable
{
    /**
     * @param Collection $collection
     * @param string $moduleName
     * @param bool $modalAction
     * @param bool $approveAction
     * @param bool $rejectAction
     * @return JsonResponse
     */
    public static function buildDatatable(
        Collection $collection,
        string $moduleName,
        bool $modalAction = false,
        bool $approveAction = false,
        bool $rejectAction = false): JsonResponse
    {
        return datatables()
            ->collection($collection)
            ->addIndexColumn()
            ->addColumn('name', function ($data) {
                return $data->citizen ? $data->citizen->name : $data->name;
            })
            ->addColumn('action', function ($data) use ($moduleName, $modalAction, $approveAction, $rejectAction) {

                $approveAction = ($approveAction && ($data->status === 'pending' || $data->status === 'rejected'));
                $rejectAction = ($rejectAction && ($data->status === 'pending' || $data->status === 'approved'));

                return view('partials.datatable-actions',
                    [
                        'data' => $data,
                        'moduleName' => $moduleName,
                        'modalAction' => $modalAction ? $data->id : false,
                        'approveAction' => $approveAction ? $data->id : false,
                        'rejectAction' => $rejectAction ? $data->id : false
                    ]
                );
            })
            ->rawColumns(['action'])
            ->make(true);
    }
}