<?php
declare(strict_types=1);

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    public $timestamps = false;

    public $fillable = [
        'slug',
        'name',
        'permissions'
    ];

    public $casts = [
        'permissions' => 'array',
    ];
}