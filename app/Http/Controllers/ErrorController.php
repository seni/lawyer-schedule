<?php

namespace App\Http\Controllers;

class ErrorController extends Controller
{
    /**
     * Page not found
     * @return \Illuminate\View\View
     */
    public function error404()
    {
        return view('errors.404');
    }

    /**
     * Access denied
     * @return \Illuminate\View\View
     */
    public function error403()
    {
        return view('errors.403');
    }
}
