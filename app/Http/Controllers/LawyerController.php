<?php
declare(strict_types=1);

namespace App\Http\Controllers;

use App\User;
use Illuminate\View\View;
use App\Helpers\Datatable;
use Illuminate\Http\JsonResponse;

class LawyerController extends Controller
{
    public $module = 'lawyers';

    /**
     * @return View
     */
    public function index(): View
    {
        $response['module'] = $this->module;
        $response['columns'] = response()->json($this->columns());
        $response['ajaxRoute'] = route($this->module . '.datatable');

        return view($this->module . '.index', $response);
    }

    /**
     * @return JsonResponse
     */
    public function datatable(): JsonResponse
    {
        if (request()->ajax()) {
            $users = User::getLawyers();

            return Datatable::buildDatatable($users, $this->module, true);
        }
    }

    /**
     * @return array
     */
    protected function columns(): array
    {
        return [
            [
                'name' => 'id',
                'slug' => 'id',
                'data' => 'id',
                'width' => '9%'
            ],
            [
                'name' => trans($this->module . '.columns.name'),
                'slug' => 'name',
                'data' => 'name',
                'width' => '60%'
            ],
            [
                'name' => trans($this->module . '.columns.actions'),
                'slug' => 'action',
                'data' => 'action',
                'width' => '280px',
                'orderable' => 'false',
                'searchable' => 'false'
            ]
        ];
    }
}
