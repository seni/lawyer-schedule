<?php
declare(strict_types=1);

namespace App\Http\Controllers;

use App\Schedule;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class ScheduleController extends Controller
{
    const PENDING_COLOR = '#f7ad06';
    const APPROVED_COLOR = '#1dbd00';
    const REJECTED_COLOR = '#ff0101';

    /**
     * @param $id
     * @return JsonResponse
     */
    public function getSchedule($id): JsonResponse
    {
        if (!request()->ajax()) {
            return response()->json(['status' => 'error']);
        }

        $parameters = request()->all();
        $startDate = !empty($parameters["start"]) ? $parameters["start"] : '';
        $endDate = !empty($parameters["end"]) ? $parameters["end"] : '';

        $data = Schedule::getMeetingRequests($id, $startDate, $endDate);

        $data = $data->mapWithKeys(function ($item, $key) {
            return [$key => [
                'id' => $item->id,
                'title' => $item->citizen->name,
                'start' => $item->meeting_date,
                'end' => Carbon::parse($item->meeting_date)->addHour()->format('Y-m-d H:i:s'),
                'color' => $this->getColor($item->status)
            ]];
        });

        return response()->json($data);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function meetingRequest(Request $request): JsonResponse
    {
        if (!$request->ajax()) {
            return response()->json(['status' => 'error']);
        }

        $citizenId = Auth::id();
        $response = $this->validateRequest($request);
        if (!empty($response)) {
            try {
                $id = Schedule::insertGetId([
                    'lawyer_id' => $response['lawyerId'],
                    'citizen_id' => $citizenId,
                    'meeting_date' => $response['start']
                ]);
            } catch (\Exception $exception) {
                logger($exception);
                return response()->json(['status' => 'error'], $exception->getCode());
            }
            return response()->json(['status' => 'success', 'id' => $id]);
        }
        return response()->json(['status' => 'error']);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function deleteRequest(Request $request): JsonResponse
    {
        if (!$request->ajax()) {
            return response()->json(['status' => 'error']);
        }

        $id = $request->get('id');
        $meeting = Schedule::find($id);

        if ($meeting->citizen_id === Auth::id()) {
            try {
                $meeting->delete();
            } catch (\Exception $exception) {
                logger($exception);
                return response()->json(['status' => 'error'], $exception->getCode());
            }
            return response()->json(['status' => 'success']);
        }
        return response()->json(['status' => 'error']);
    }

    /**
     * @param string $status
     * @return string
     */
    protected function getColor(string $status): string
    {
        if ($status === 'pending')
            return self::PENDING_COLOR;

        if ($status === 'approved')
            return self::APPROVED_COLOR;

        if ($status === 'rejected')
            return self::REJECTED_COLOR;
    }

    /**
     * @param Request $request
     * @return array
     */
    private function validateRequest(Request $request): array
    {
        $validator = Validator::make($request->all(), [
            'lawyerId' => 'required|integer',
            'start' => 'required|date|after:today',
            'end' => 'required|date|after:today',
        ]);

        if ($validator->fails()) {
            return [];
        }

        return $validator->validated();
    }
}
