<?php
declare(strict_types=1);

namespace App\Http\Controllers;

use App\Schedule;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;
use App\Helpers\Datatable;
use Illuminate\Http\JsonResponse;

class CitizenController extends Controller
{
    public $module = 'citizens';

    /**
     * @return View
     */
    public function index(): View
    {
        $response['module'] = $this->module;
        $response['columns'] = response()->json($this->columns());
        $response['ajaxRoute'] = route($this->module . '.datatable');

        return view($this->module . '.index', $response);
    }

    /**
     * @return JsonResponse
     */
    public function datatable(): JsonResponse
    {
        if (!request()->ajax()) {
            return response()->json(['status' => 'error']);
        }
        $lawyerId = (int)Auth::user()->id;
        $meetings = Schedule::getMeetingRequests($lawyerId, today(), today()->addDays(8));
        return Datatable::buildDatatable($meetings, $this->module, false, true, true);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function update(Request $request): JsonResponse
    {
        if (!request()->ajax()) {
            return response()->json(['status' => 'error']);
        }

        $id = $request->get('id');
        $status = $request->get('status');
        try {
            Schedule::find($id)->update(['status' => $status]);
        } catch (\Exception $exception) {
            logger($exception);
            return response()->json(['status' => 'error']);
        }

        return response()->json(['status' => 'success']);
    }

    /**
     * @return array
     */
    protected function columns(): array
    {
        return [
            [
                'name' => 'id',
                'slug' => 'id',
                'data' => 'id',
                'width' => '9%'
            ],
            [
                'name' => trans($this->module . '.columns.name'),
                'slug' => 'name',
                'data' => 'name',
                'width' => '30%'
            ],
            [
                'name' => trans($this->module . '.columns.meetingDate'),
                'slug' => 'meeting_date',
                'data' => 'meeting_date',
                'width' => '10%'
            ],
            [
                'name' => trans($this->module . '.columns.status'),
                'slug' => 'status',
                'data' => 'status',
                'width' => '10%'
            ],
            [
                'name' => trans($this->module . '.columns.actions'),
                'slug' => 'action',
                'data' => 'action',
                'width' => '280px',
                'orderable' => 'false',
                'searchable' => 'false'
            ]
        ];
    }
}
