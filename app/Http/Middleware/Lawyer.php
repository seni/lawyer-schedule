<?php
declare(strict_types=1);

namespace App\Http\Middleware;

use Closure;
use App\Helpers\Access;
use Illuminate\Http\Request;

class Lawyer
{
    private $lawyerSlug = 'lawyer';

    /**
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $access = new Access($this->lawyerSlug);
        $accessAllowed = $access->checkPermissions($request);

        if (!$accessAllowed) {
            return redirect()->route('error.403');
        }

        return $next($request);
    }
}
