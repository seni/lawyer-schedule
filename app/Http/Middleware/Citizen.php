<?php
declare(strict_types=1);

namespace App\Http\Middleware;

use Closure;
use App\Helpers\Access;
use Illuminate\Http\Request;

class Citizen
{
    private $citizenSlug = 'citizen';

    /**
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $access = new Access($this->citizenSlug);
        $accessAllowed = $access->checkPermissions($request);

        if (!$accessAllowed) {
            return redirect()->route('error.403');
        }

        return $next($request);
    }
}
