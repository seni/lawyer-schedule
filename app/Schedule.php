<?php
declare(strict_types=1);

namespace App;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Schedule extends Model
{
    public $table = 'schedule';

    public $fillable = [
        'lawyer_id',
        'citizen_id',
        'meeting_date',
        'status'
    ];

    public $casts = [
        'status' => 'enum'
    ];

    /**
     * @return BelongsTo
     */
    public function citizen(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @param $lawyerId
     * @param $selectedDate
     * @return mixed
     */
    public static function getSchedule($lawyerId, $selectedDate)
    {
        return self::where('lawyer_id', $lawyerId)->whereDate('meeting_date', $selectedDate)->get();
    }

    /**
     * @param int $lawyerId
     * @param string $startDate
     * @param string $endDate
     * @return Collection
     */
    public static function getMeetingRequests(int $lawyerId, $startDate, $endDate): Collection
    {
        return self::with('citizen')
            ->where('lawyer_id', $lawyerId)
            ->whereBetween('meeting_date', [$startDate, $endDate])
            ->get();
    }
}