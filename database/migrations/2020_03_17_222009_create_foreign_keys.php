<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Eloquent\Model;

class CreateForeignKeys extends Migration
{

    public function up()
    {
        Schema::table('user_role', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
        Schema::table('user_role', function (Blueprint $table) {
            $table->foreign('role_id')->references('id')->on('roles')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
        Schema::table('schedule', function (Blueprint $table) {
            $table->foreign('lawyer_id')->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
        Schema::table('schedule', function (Blueprint $table) {
            $table->foreign('citizen_id')->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    public function down()
    {
        Schema::table('user_role', function (Blueprint $table) {
            $table->dropForeign('user_role_user_id_foreign');
        });
        Schema::table('user_role', function (Blueprint $table) {
            $table->dropForeign('user_role_role_id_foreign');
        });
        Schema::table('schedule', function (Blueprint $table) {
            $table->dropForeign('schedule_lawyer_id_foreign');
        });
        Schema::table('schedule', function (Blueprint $table) {
            $table->dropForeign('schedule_citizen_id_foreign');
        });
    }
}