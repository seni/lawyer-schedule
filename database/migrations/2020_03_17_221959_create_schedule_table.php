<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateScheduleTable extends Migration {

    public function up()
    {
        Schema::create('schedule', function(Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('lawyer_id')->unsigned();
            $table->bigInteger('citizen_id')->unsigned();
            $table->timestamp('meeting_date');
            $table->enum('status', array('pending', 'approved', 'rejected'))->default('pending');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('schedule');
    }
}