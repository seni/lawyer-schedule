<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRolesTable extends Migration {

	public function up()
	{
		Schema::create('roles', function(Blueprint $table) {
			$table->increments('id');
			$table->string('slug')->unique();
			$table->string('name');
			$table->json('permissions');
		});
	}

	public function down()
	{
		Schema::drop('roles');
	}
}