<?php

use App\Role;
use App\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
//            'RulesTableSeeder'
//            PostsTableSeeder::class,
        ]);

        Role::insert([
            [
                'slug' => 'admin',
                'name' => 'Admin',
                'permissions' => json_encode(['home' => true])
            ], [
                'slug' => 'lawyer',
                'name' => 'Lawyer',
                'permissions' => json_encode([
                    'home' => true,
                    'citizens.index' => true,
                    'citizens.update' => true,
                    'citizens.datatable' => true
                ])
            ], [
                'slug' => 'citizen',
                'name' => 'Citizen',
                'permissions' => json_encode([
                    'home' => true,
                    'lawyers.index' => true,
                    'lawyers.datatable' => true,
                    'schedule.getSchedule' => true,
                    'schedule.deleteRequest' => true,
                    'schedule.meetingRequest' => true
                ])
            ]
        ]);

        $adminRoleId = Role::where('slug', 'admin')->first()->id;

        User::create([
            'name' => 'Admin',
            'email' => 'admin@example.com',
            'password' => bcrypt('secret')
        ])->roles()->sync([$adminRoleId]);
    }
}
