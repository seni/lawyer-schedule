<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/404', 'ErrorController@error404')->name('error.404');
Route::get('/403', 'ErrorController@error403')->name('error.403');

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::as('admin.')->middleware(['auth', 'admin'])->prefix('admin')->group(function () {
    Route::get('/home', 'HomeController@index')->name('dashboard');
});

Route::as('citizens.')->middleware(['auth', 'lawyer'])->prefix('citizens')->group(function () {
    Route::get('/getCitizens', 'CitizenController@datatable')->name('datatable');
    Route::get('/', 'CitizenController@index')->name('index');
    Route::patch('/update', 'CitizenController@update')->name('update');

});

Route::middleware(['auth', 'citizen'])->prefix('lawyers')->group(function () {

    Route::as('lawyers.')->group(function () {
        Route::get('/getLawyers', 'LawyerController@datatable')->name('datatable');
        Route::get('/', 'LawyerController@index')->name('index');
    });

    Route::as('schedule.')->group(function () {
        Route::get('/getSchedule/{id}', 'ScheduleController@getSchedule')->name('getSchedule');
        Route::post('/meetingRequest', 'ScheduleController@meetingRequest')->name('meetingRequest');
        Route::delete('/deleteRequest', 'ScheduleController@deleteRequest')->name('deleteRequest');
    });
});
