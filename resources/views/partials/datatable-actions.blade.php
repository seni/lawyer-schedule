@if($approveAction)
    <button type="button" class="btn btn-success btn-sm approve" data-id="{{ $approveAction }}">
        {{ trans($moduleName . '.actions.approve') }}
    </button>
@endif

@if($rejectAction)
    <button type="button" class="btn btn-danger btn-sm reject" data-id="{{ $rejectAction }}">
        {{ trans($moduleName . '.actions.reject') }}
    </button>
@endif

@if($modalAction)
    <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#modal-{{ $moduleName }}"
            data-id="{{ $modalAction }}">
        {{ trans($moduleName . '.actions.meetingRequest') }}
    </button>
@endif
