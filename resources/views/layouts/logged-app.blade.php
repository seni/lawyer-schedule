<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Styles -->
    <link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">


    <script src="{{ asset('js/app.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.9.0/fullcalendar.css"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"
            integrity="sha256-4iQZ6BVL4qNKlQ27TExEhBN1HFPvAvAMbFavKKosSWQ=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.9.0/fullcalendar.js"></script>
</head>
<body>
<div id="app">
    <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
        <div class="container">
            <a class="navbar-brand" href="{{ url('/') }}">
                {{ config('app.name', 'Laravel') }}
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false"
                    aria-label="{{ __('Toggle navigation') }}">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <!-- Left Side Of Navbar -->
                <ul class="navbar-nav mr-auto">

                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="navbar-nav ml-auto">
                    <!-- Authentication Links -->
                    @guest
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                        </li>
                        @if (Route::has('register'))
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                            </li>
                        @endif
                    @else
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                      style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
                    @endguest
                </ul>
            </div>
        </div>
    </nav>

    <main class="py-4">
        @yield('content')
    </main>
</div>
</body>
@auth
    <!-- Scripts -->
    <script>
        $(function () {
            let currentCitizenName = "{{ auth()->user()->name }}";
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            let alert_success = $('#{{ $module }}-success');
            let alert_error = $('#{{ $module }}-error');
            let columns = "{{ json_encode($columns->getData())}}";
            $('.data-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ $ajaxRoute }}",
                columns: JSON.parse(columns.replace(/&quot;/g, '"')),
                drawCallback: function () {
                    $(".approve, .reject").on('click', function () {
                        let id = $(this).attr('data-id');
                        let status = $(this).hasClass('approve') ? 'approved' : 'rejected';
                        $.ajax({
                            url: '/citizens/update/',
                            data: {
                                id: id,
                                status: status
                            },
                            type: "PATCH",
                            success: function (data) {
                                $('.data-table').DataTable().ajax.reload();
                            }
                        });
                    });
                }
            });

            alert_success.hide();
            alert_error.hide();

            var modal_lawyers = $('.modal#modal-lawyers');
            modal_lawyers.on('shown.bs.modal', function (event) {

                var start = new Date();
                var end = new Date(start.getFullYear(), start.getMonth(), start.getDate() + 8);
                var id = $(event.relatedTarget).attr('data-id');
                var calendar = $('#calendar').fullCalendar({
                    events: '/lawyers/getSchedule/' + id,
                    defaultView: 'agenda',
                    header: {
                        left: '',
                        center: 'title',
                        right: ''
                    },
                    visibleRange: {
                        start: start,
                        end: end
                    },
                    slotDuration: '01:00:00',
                    minTime: '09:00:00',
                    maxTime: '19:00:00',
                    agendaEventMinHeight: 23,
                    eventOverlap: false,
                    slotEventOverlap: false,
                    displayEventTime: true,
                    weekends: false,
                    eventRender: function (event) {
                        event.allDay = false;
                    },
                    selectable: true,
                    select: function (start, end, allDay) {
                        var start = $.fullCalendar.formatDate(start, "Y-MM-DD HH:mm:ss");
                        var end = $.fullCalendar.formatDate(end, "Y-MM-DD HH:mm:ss");

                        $.ajax({
                            url: "{{ route('schedule.meetingRequest') }}",
                            data: {
                                lawyerId: id,
                                start: start,
                                end: end
                            },
                            type: "POST",
                            success: function (data) {
                                displayMessage("Added Successfully");
                                calendar.fullCalendar('renderEvent',
                                    {
                                        id: data.id,
                                        title: currentCitizenName,
                                        start: start,
                                        end: end,
                                        color: '#05adff',
                                        allDay: allDay
                                    },
                                    true
                                );
                            }
                        });

                        calendar.fullCalendar('unselect');
                    },
                    eventClick: function (event) {
                        console.log(event);
                        var deleteMsg = confirm("Do you really want to delete?");
                        if (deleteMsg) {
                            $.ajax({
                                type: "DELETE",
                                url: "{{ route('schedule.deleteRequest') }}",
                                data: {
                                    id: event.id
                                },
                                success: function (response) {
                                    if (response.status === 'success') {
                                        $('#calendar').fullCalendar('removeEvents', event.id);
                                        displayMessage("Deleted Successfully");
                                    }
                                }
                            });
                        }
                    }
                });
            });
            modal_lawyers.on('hidden.bs.modal', function (e) {
                $('#calendar').fullCalendar('destroy');
            })
        });

        function displayMessage(message) {
            $(".response").html("<div class='text-success'>" + message + "</div>");
            setInterval(function () {
                $(".success").fadeOut();
            }, 1000);
        }
    </script>

    @endauth

    </body>
</html>
