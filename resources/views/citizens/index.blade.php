@extends('layouts.logged-app')

@section('content')
    <div class="container">
        <div class="card border-info mb-3">
            <div class="card-header text-white bg-info border-info">
                <h4>{{ trans($module . '.moduleName') }}</h4>
            </div>
            <div class="col-12">
                <div class="alert alert-success" id="{{ $module }}-success" role="alert">
                    {{ trans($module . '.messages.successfullyCreated') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
            <div class="card-body">
                <div class="col-12 mt-3 table-responsive">
                    <table class="table table-bordered data-table">
                        <thead>
                        <tr>
                            @foreach($columns->getData() as $key => $value)
                                <th width="{{$value->width}}">{{ $value->name }}</th>
                            @endforeach
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
