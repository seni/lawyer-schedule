<?php

return [
    'moduleName' => 'Lawyers',
    'columns' => [
        'id' => 'Id',
        'name' => 'Name',
        'actions' => 'Actions',
    ],
    'actions' => [
        'approve' => 'Approve',
        'reject' => 'Reject',
        'meetingRequest' => 'Meeting request',
    ]
];